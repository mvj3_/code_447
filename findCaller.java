public static void findCaller() {
		final Throwable mThrowable = new Throwable();
		final StackTraceElement[] elements = mThrowable.getStackTrace();
		final int len = elements.length;
		StackTraceElement item = null;
		for (int i = 0; i < len; i++) {
			item = elements[i];
			Log.e("findCaller", "Position: " + item.getClassName() + "."
					+ item.getMethodName() + " ---" + item.getLineNumber()
					+ " line");
		}
	}